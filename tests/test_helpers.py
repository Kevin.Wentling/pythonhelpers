"""Unit tests for helpers."""

import unittest
import logging
import os

import helpers

from . import test_setup

# Disable logging for tests
logging.disable(logging.CRITICAL)

class TestConfigParser(test_setup.TestsSetup):
    """Functions to test functionality in config_parser."""

    def setUp(self):
        """Initial setup to run before every test."""
        super().setUp()

    def test_load_json_valid(self):
        json = helpers.load_json(os.path.join(self.fixtures, 'instances.json'))
        self.assertEqual(type(json), dict)

    def test_load_json_invalid(self):
        json = helpers.load_json(os.path.join(self.fixtures, 'instances.txt'))
        self.assertEqual(json, {})

    def test_find_keys_valid_dict(self):
        example_dict =  {
                          'key1': {
                              'key2': 'value2'
                          }
                        }
        result = helpers.find_keys(example_dict, 'key2')
        self.assertEqual(result, ['value2'])

    def test_find_keys_invalid_dict(self):
        example_dict =  {
                          'key1': {
                              'key2': 'value2'
                          }
                        }
        result = helpers.find_keys(example_dict, 'key3')
        self.assertEqual(result, [])

    def test_find_keys_valid_list(self):
        example_dict =  [
                          {
                            'key1': {
                                'key2': 'value2'
                            }
                          },
                          {
                            'key3': {
                                'key2': 'value4'
                            }
                          }
                        ]
        result = helpers.find_keys(example_dict, 'key2')
        self.assertEqual(result, ['value2', 'value4'])

    def test_find_keys_invalid_list(self):
        example_dict =  [
            {
                'key1': {
                    'key2': 'value2'
                }
            },
            {
                'key3': {
                    'key2': 'value4'
                }
            }
        ]
        result = helpers.find_keys(example_dict, 'key5')
        self.assertEqual(result, [])

    def test_fallten_data_dict(self):
        example_dict =  {
            'key1': {
                'key2': 'value2'
            },
            'key3': {
                'key2': 'value4'
            }
        }
        expected_result = {
            'key1_key2': 'value2',
            'key3_key2': 'value4'
        }
        result = helpers.flatten_data(example_dict)
        self.assertEqual(result, expected_result)

    def test_fallten_data_array(self):
        example_dict =  [
            {
                'key1': {
                    'key2': 'value2'
                }
            },
            {
                'key3': {
                    'key2': 'value4'
                }
            }
        ]
        expected_result = {
            '0_key1_key2': 'value2',
            '1_key3_key2': 'value4'
        }
        result = helpers.flatten_data(example_dict)
        self.assertEqual(result, expected_result)

    def test_consolidate_fattened_data(self):
        example_dict = {
            'Tags_0_Key': 'platform',
            'Tags_0_Value': 'some_platform',
            'Tags_1_Key': 'EngTeam',
            'Tags_1_Value': 'some_team',
            'Tags_2_Key': 'Name',
            'Tags_2_Value': 'some_name',
            'Tags_3_Key': 'aws_environment',
            'Tags_3_Value': 'some_env'
        }
        expected_result = {
            'platform': 'some_platform',
            'EngTeam': 'some_team',
            'Name': 'some_name',
            'aws_environment': 'some_env'
        }
        result = helpers.consolidate_fattened_data(example_dict, 'tags', 'key', 'value')
        self.assertEqual(result, expected_result)

    def test_remove_keys_from_dict_fuzzy(self):
        example_dict = {
            'Tags_0_Key': 'platform',
            'Tags_0_Value': 'some_platform',
            'Tags_1_Key': 'EngTeam',
            'Tags_1_Value': 'some_team',
            'Tags_2_Key': 'Name',
            'Tags_2_Value': 'some_name',
            'Tags_3_Key': 'aws_environment',
            'Tags_3_Value': 'some_env'
        }
        expected_result = {}
        result = helpers.remove_keys_from_dict_fuzzy(example_dict, 'Tags')
        self.assertEqual(result, expected_result)

    def test_remove_key_from_dict(self):
        example_dict = {
            'Tags_0_Key': 'platform',
            'Tags_0_Value': 'some_platform',
            'Tags_1_Key': 'EngTeam',
            'Tags_1_Value': 'some_team',
            'Tags_2_Key': 'Name',
            'Tags_2_Value': 'some_name',
            'Tags_3_Key': 'aws_environment',
            'Tags_3_Value': 'some_env'
        }
        expected_result = {
            'Tags_0_Key': 'platform',
            'Tags_0_Value': 'some_platform',
            'Tags_1_Key': 'EngTeam',
            'Tags_1_Value': 'some_team',
            'Tags_2_Key': 'Name',
            'Tags_2_Value': 'some_name',
            'Tags_3_Value': 'some_env'
        }
        result = helpers.remove_key_from_dict(example_dict, 'Tags_3_Key')
        self.assertEqual(result, expected_result)

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()
