"""Unit tests for config_parser."""

import unittest
import logging
from os.path import join
from pathlib import Path

class TestsSetup(unittest.TestCase):
    """Generic setup for tests."""

    def setUp(self):
        """Initial setup to run before every test."""
        self.fixtures = join(Path(__file__).parent.absolute(), 'fixtures')
        logging.disable(logging.CRITICAL)

    def tearDown(self):
        pass

#if __name__ == '__main__':
#    unittest.main()
