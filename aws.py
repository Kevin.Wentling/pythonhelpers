"""Library to interact with AWS."""

from datetime import datetime
from logging import exception
import boto3
import botocore
import json
import pyjq
import _pyjq

from . import logger
from . import helpers

log = logger.getLogger(__name__)

class DatetimeEncoder(json.JSONEncoder):
  def default(self, obj):
    if isinstance(obj, datetime):
        return obj.strftime('%Y-%m-%dT%H:%M:%SZ')
    elif isinstance(obj, date):
        return obj.strftime('%Y-%m-%d')
    # Let the base class default method raise the TypeError
    return json.JSONEncoder.default(self, obj) 

def convert_timestamps_in_dict(instances):
    """Converts timestamps in returned dict."""
    return json.loads(json.dumps(instances, cls=DatetimeEncoder))

def get_existing_session(session):
    try:
        session = boto3.session.Session(profile_name=session)
    except botocore.exceptions.ProfileNotFound:
        log.error('Could not find profile {}'.format(session))

    if not session:
        return

    return session


def get_base_session():

    try:
        session = boto3.Session(region_name='eu-west-1')

        # Test if the credentials are valid as they are automatically fetched on EC2 instances
        ec2 = get_ec2_client(session)
        ec2.describe_regions()
    except:
        return None

    return session

def get_session(session, mfa_totp=None, interactive=True, session_validity=900):
    """Create a session with temporary credentials."""

    log.info('Creating temporary session')
    log.debug('Get user MFA serial number')
    mfa = session.client('iam').list_mfa_devices()['MFADevices'][0]['SerialNumber']

    if interactive:
        log.debug('Interactive session set, asking for TOTP')
        mfa_totp = input('OTP Code: ')

    log.debug('Generating temporary credentials valid for {} seconds'.format(session_validity))

    if mfa_totp:
        log.debug('Using TOTP')
        tokens = session.client('sts').get_session_token(
            DurationSeconds=session_validity,
            SerialNumber=mfa,
            TokenCode=mfa_totp
        )
    else:
        # Untested
        log.debug('Not using TOTP')
        tokens = session.client('sts').get_session_token(
            DurationSeconds=session_validity
        )

    log.debug('Setting temporary session credentials')
    session = boto3.Session(
        aws_access_key_id=tokens['Credentials']['AccessKeyId'],
        aws_secret_access_key=tokens['Credentials']['SecretAccessKey'],
        aws_session_token=tokens['Credentials']['SessionToken']
    )

    if not session:
        log.error('Failed to create session')

    log.info('Temporary session created')

    return session

def get_profile_session(profile):
    """Set AWS profile to use."""

    log.info('Loading AWS profile {}'.format(profile))

    session = None

    try:
        session = boto3.session.Session(profile_name=profile)
    except botocore.exceptions.ProfileNotFound:
        log.error('Could not find profile {}'.format(profile))

    if not session:
        return

    session = get_session(session, interactive=True)

    return session

def assume_role(arn, name_prefix):
    """Assume a specific role."""
    sts = boto3.client('sts')
    assumed_role_object = sts.assume_role(
        RoleArn = arn,
        RoleSessionName = "{}-nessus-script".format(name_prefix)
    )
    credentials = assumed_role_object['Credentials']

    session = boto3.Session(
        region_name = 'eu-west-1',
        aws_access_key_id = credentials['AccessKeyId'],
        aws_secret_access_key = credentials['SecretAccessKey'],
        aws_session_token = credentials['SessionToken'],
    )

    return session

def get_regions(session):
    """Fetch all available EC2 regions."""

    ec2 = session.client('ec2')

    log.debug('Fetching EC2 regions')
    response = ec2.describe_regions()

    if not response:
        log.error('No regions found')
        return

    log.debug('EC2 regions fetched')
    log.debug(response['Regions'])

    _results = pyjq.all('.Regions[].RegionName', response)

    return _results

def get_ec2_client(session, region=None):
    """Get EC2 region object."""

    if not type(session) == boto3.session.Session:
        log.error('Boto3 session not provided')
        return

    if region:
        log.debug('Creating EC2 object for region {}'.format(region))
        ec2 = session.client('ec2', region_name=region)
        if not ec2:
            log.error('Failed to create EC2 client for region {}'.format(region))
            return
        log.debug('EC2 object created')
        return ec2

    log.debug('Creating EC2 object for region default region')
    try:
        ec2 = session.client('ec2')
    except botocore.exceptions.NoRegionError as e:
        log.error('Region not specified when creating EC2 client')
        return
    if not ec2:
        log.error('Failed to create EC2 client for default region')
        return
    log.debug('EC2 object created')
    return ec2

def get_ec2_instances(ec2, NextToken='', filters=[], MaxResults=10):
    """Get EC2 instances."""

    try:
        ec2._client_config.region_name
    except AttributeError:
        log.error('Boto3 EC2 client not provided ')
        return

    log.debug('Fetching EC2 instances in region {}'.format(ec2._client_config.region_name))

    instances = ec2.describe_instances(Filters=filters, MaxResults=MaxResults, NextToken=NextToken)

    if not instances:
        log.error('Failed to fetch instances')
        return

    log.debug('{} instances fetched'.format(MaxResults))

    return instances

def get_all_ec2_instances(ec2, status='running'):
    """Get all instance IDs."""
    
    result = []
    fiters = []
    NextToken = ''

    if status == 'running':
        filters=[{'Name':'instance-state-name', 'Values': ['running']}]
    elif status == 'stopped':
        filters=[{'Name':'instance-state-name', 'Values': ['stopped']}]

    while True:
        response = get_ec2_instances(ec2, filters=filters, NextToken=NextToken)
        response = convert_timestamps_in_dict(response)
        result += pyjq.all(".Reservations[].Instances[].InstanceId", response)
        if 'NextToken' not in response:
            break

        NextToken = response['NextToken']

    return result

def get_ec2_instance(ec2, instanceid):
    """Fetch information about one EC2 instance."""

    try:
        ec2._client_config.region_name
    except AttributeError:
        log.error('Boto3 EC2 client not provided ')
        return

    response = ec2.describe_instances(InstanceIds=[instanceid])

    if not response:
        log.error('Failed to fetch {} instance details.'.format(instanceid))

    response = convert_timestamps_in_dict(response)

    return response

def get_ec2_instance_private_ips(instance_data):
    """Fetch only Private IPs from instance."""

    private_ips = pyjq.all('.Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress', instance_data)
    return private_ips

def get_ec2_instance_public_ips(instance_data):
    """Fetch only Private IPs from instance."""

    public_ips = pyjq.all('.Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].Association.PublicIp', instance_data)
    return public_ips

def get_ec2_instance_details(instance_data):
    """Extract IPs and name from instance."""

    result = {}
    private_ips = None
    public_ips = None
    name = None
    instanceid = None

    try:
        instanceid = pyjq.all(".Reservations[].Instances[].InstanceId", instance_data)
    except:
        log.critical('Failed to find Instance ID in instance data')

    private_ips = pyjq.all('.Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress', instance_data)
    public_ips = pyjq.all('.Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].Association.PublicIp', instance_data)
    
    try:
        name = pyjq.all('.Reservations[].Instances[].Tags[] | select(.Key=="Name").Value', instance_data)
        if not name:
            name = pyjq.all('.Reservations[].Instances[].Tags[] | select(.Key=="eks:cluster-name").Value', instance_data)
    except _pyjq.ScriptRuntimeError:
        log.warning('No tags configured on {}'.format(instanceid))
        

    private_ips = helpers.remove_duplicates_from_list(private_ips)
    private_ips = helpers.remove_none_from_list(private_ips)

    public_ips = helpers.remove_duplicates_from_list(public_ips)
    public_ips = helpers.remove_none_from_list(public_ips)

    if private_ips:
        result.update({
            'private_ips': private_ips
            }
        )

    if public_ips:
        result.update({
            'public_ips': public_ips
            }
        )

    if name:
        result.update({'name': name})
    else:
        log.warning('No name set on {}'.format(instanceid))

    return result


def get_ec2_subnets(ec2):
    """Fetch EC2 subnets."""

    log.info('Fetching subnets')
    subnets = ec2.describe_subnets()

    if not subnets:
        log.error('Failed to fetch subnets')

    log.debug(subnets)

    return subnets

def get_ec2_vpcs(ec2):
    """Get VPCs in EC2 region."""

    log.info('Fetching VPCs')

    vpcs = ec2.describe_vpcs()

    if not vpcs:
        log.error('Failed to fetch VPCs')

    log.debug(vpcs)

    vpcs = convert_timestamps_in_dict(vpcs)

    return vpcs
    
def get_ec2_subnets(ec2):
    """Get subnets in EC2 region."""    

    log.info('Fetching subnets')

    subnets = ec2.describe_subnets()

    if not subnets:
        log.error('Failed to fetch subnets')

    log.debug(subnets)

    subnets = convert_timestamps_in_dict(subnets)

    return subnets

def get_vpc_info(vpcs):
    """Returns CIDR, VPC ID and Tags for provided VPCs."""
    
    results = {}

    _results = pyjq.all('.Vpcs[] | { CidrBlock, VpcId, Tags }', vpcs)

    for _result in _results:
        name = None
        for tag in _result['Tags']:
            if tag['Key'] == 'Name':
                name = tag['Value']
        if name:
            results.update({_result['VpcId']: {'CidrBlock': _result['CidrBlock'], 'Name': name}})
        else:
            results.update({_result['VpcId']: {'CidrBlock': _result['CidrBlock']}})

    return results

def get_subnet_info(subnets):
    """Returns AvailabilityZone, CidrBlock, VpcId, Tags."""
    
    return pyjq.all('.Subnets[] | { AvailabilityZone, CidrBlock, VpcId, Tags }', subnets)

def get_ec2_addresses(ec2):
    """Get addresses in EC2 region."""

    log.info('Fetching addresses')

    addresses = ec2.describe_network_interfaces()

    if not addresses:
        log.error('Failed to fetch subnets')

    log.debug(addresses)

    addresses = convert_timestamps_in_dict(addresses)

    return addresses

def get_private_addresses(addresses):
    """Returns private addresses from dict of addresses."""

    _results = pyjq.all('.NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress', addresses)

    return _results

def get_public_addresses(addresses):
    """Returns public addresses from dict of addresses."""

    _results = pyjq.all('.NetworkInterfaces[].PrivateIpAddresses[].Association.PublicIp', addresses)

    return _results

